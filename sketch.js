// Define the array to be searched and the target value
let values = [];
let target = 0;
let found = false;
let currentIndex = 0;

function setup() {
  createCanvas(800, 400);
  for (let i = 0; i < 50; i++) {
    values.push(floor(random(100)));
  }
  values.sort((a, b) => a - b); // Sort the array for easier visualization
  target = random(100);
  linearSearch();
}

function draw() {
  background(220);

  // Display the array elements
  for (let i = 0; i < values.length; i++) {
    if (i === currentIndex) {
      fill(255, 0, 0); // Highlight the current element being compared
    } else {
      fill(0, 0, 0);
    }
    rect(i * 15, height / 2, 10, -values[i] * 4);
  }

  // Show if the target was found
  if (found) {
    fill(0, 255, 0);
    textSize(32);
    text(`Target found at index ${currentIndex}`, 10, height - 20);
  } else {
    fill(0);
    textSize(32);
    text("Target not found", 10, height - 20);
  }
}

function linearSearch() {
  currentIndex = 0;
  found = false;
  function search() {
    if (currentIndex < values.length) {
      if (values[currentIndex] === target) {
        found = true;
        return;
      }
      currentIndex++;
    } else {
      found = false;
      return;
    }
  }
  let interval = setInterval(search, 500); // Adjust the interval speed
}
